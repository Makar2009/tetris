from tkinter import *
from random import randint

cell_size = 40
rows = 20
columns = 9
border_width = 10
speed = 1000

x0 = border_width - 1
y0 = border_width - 1
xe = border_width + cell_size * columns + 1
ye = border_width + cell_size * rows + 1

root = Tk()
c = Canvas(width=border_width * 1.5 + columns // 2 * cell_size + columns * cell_size,
           height=border_width * 1.5 + rows * cell_size, bg='white')
c.focus_set()
c.pack()

figures = [
    [(0, 0), (0, 1), (0, 2), (0, 3)],  # линния из 4
    [(1, 0), (0, 1), (0, 0), (0, 2)],  # фигура "г"
    [(0, 0), (1, 1), (1, 0), (1, 2)],  # фигура "г" перевёрнутая
    [(0, 0), (0, 1), (1, 0), (1, 1)],  # квадрат 2x2
    [(0, 0), (0, 1), (1, 1), (0, 2)],  # финура "трон"
    [(0, 0), (0, 1), (1, 1), (1, 2)],  # фигура "змейка"
    [(1, 0), (1, 1), (0, 1), (0, 2)]  # фигура "змейка" перевёрнутая
]

class Figure():
    def __init__(self, figure_id, coord1, coord2):
        self.blocks = []
        self.figure_id = figure_id
        color = ['red', 'orange', 'yellow', 'green', 'blue', 'dark blue', 'purple']
        for i in figures[self.figure_id]:
            self.blocks.append(
                c.create_rectangle(
                    border_width + columns // 2 * cell_size + i[0] * cell_size + coord1 * cell_size,
                    border_width + i[1] * cell_size + coord2 * cell_size,
                    border_width + columns // 2 * cell_size + cell_size + i[0] * cell_size + coord1 * cell_size,
                    border_width + cell_size + i[1] * cell_size + coord2 * cell_size,
                    fill=color[self.figure_id]
                )
            )
        return

    def fall(self, test=False):
        new_coords = []
        if test:
            for block in self.blocks:
                new_coords.append(
                    [[c.coords(block)[0], c.coords(block)[1] + cell_size],
                     [c.coords(block)[2], c.coords(block)[3] + cell_size]]
                )
            return new_coords

        for block in self.blocks:
            c.move(block, 0, cell_size)
        return

    def rotate(self, test=False):
        new_coords = []
        center = []
        center.append(c.coords(self.blocks[1])[0] + cell_size // 2)
        center.append(c.coords(self.blocks[1])[1] + cell_size // 2)

        for i in range(0, 4):
            centeri = []
            centeri.append(c.coords(self.blocks[i])[0] + cell_size // 2)
            centeri.append(c.coords(self.blocks[i])[1] + cell_size // 2)
            xc = center[1] - centeri[1]
            yc = centeri[0] - center[0]

            new_coords.append(
                [[xc - cell_size // 2 + center[0], yc - cell_size // 2 + center[1]],
                 [xc + cell_size // 2 + center[0], yc + cell_size // 2 + center[1]]]
            )
            if not test:
                c.coords(self.blocks[i], new_coords[i][0][0], new_coords[i][0][1], new_coords[i][1][0],
                         new_coords[i][1][1])
        if test:
            return new_coords
        return

    def right(self, test=False):
        new_coords = []
        if test:
            for block in self.blocks:
                new_coords.append(
                    [[c.coords(block)[0] + cell_size, c.coords(block)[1]],
                     [c.coords(block)[2] + cell_size, c.coords(block)[3]]]
                )
            return new_coords

        for block in self.blocks:
            c.move(block, cell_size, 0)
        return

    def left(self, test=False):
        new_coords = []
        if test:
            for block in self.blocks:
                new_coords.append(
                    [[c.coords(block)[0] - cell_size, c.coords(block)[1]],
                     [c.coords(block)[2] - cell_size, c.coords(block)[3]]]
                )
            return new_coords

        for block in self.blocks:
            c.move(block, -cell_size, 0)
        return

    def drop(self):
        global speed
        speed = speed // 10
        return

    def delete_blocks(self):
        for block in self.blocks:
            c.delete(block)
        return


class Tetris():
    def __init__(self):
        self.field1 = None
        self.field1 = None
        self.storetext = 0
        self.endtext = 0
        self.col_vse = 0
        self.och = 0
        self.col = 0
        self.text = 0
        self.paysa = 0
        self.speed_otn = 0
        self.next_id = randint(0, 6)
        self.next_figure = None
        self.blocks = []
        self.stop = False
        self.fallen_bl = []
        self.figure = None
        c.bind('<Key>', self.controlling)
        return

    def print_field(self):
        self.field1 = c.create_rectangle(border_width / 2, border_width / 2, columns * cell_size + border_width * 1.5,
                           rows * cell_size + border_width * 1.5, outline='green', width=border_width)
        self.field2 = c.create_rectangle(border_width / 2 + columns * cell_size + border_width * 1.5, border_width / 2,
                           columns * cell_size + border_width * 1.5 + columns // 2 * cell_size,
                           rows * cell_size + border_width * 1.5, outline='green', width=border_width)
        return

    def delete_line(self):
        col = {}
        delete = []
        for fall_block in self.fallen_bl:
            y = c.coords(fall_block)[1]
            if y in col:
                col[y] += 1
            else:
                col[y] = 1
        for y, count in sorted(col.items()):
            if count == columns:
                for fall_block in self.fallen_bl:
                    if c.coords(fall_block)[1] == y:
                        delete.append(fall_block)
                for fall_block in self.fallen_bl:
                    if c.coords(fall_block)[1] < y:
                        c.move(fall_block, 0, cell_size)
                for fall_block in delete:
                    c.delete(fall_block)
                    self.fallen_bl.remove(fall_block)
                self.col += 1
                self.col_vse += 1
                delete.clear()
        c.delete(self.text)
        return

    def score(self):
        if self.col == 1:
            self.och += 500
        elif self.col == 2:
            self.och += 1500
        elif self.col == 3:
            self.och += 4500
        elif self.col == 4:
            self.och += 13500
        self.col = 0
        return

    def is_point_fits_field(self, x, y):
        global x0, y0, xe, ye
        if x > x0 and y > y0 and x < xe and y < ye:
            return True
        return False

    def save_fallen_block(self):
        for block in self.figure.blocks:
            self.fallen_bl.append(block)
        return

    # coords = [[[x1, y1],[x2, y2]],[],[],[]]
    def check_can_move_block(self, coords):
        for block_coord in coords:
            if not self.is_point_fits_field(block_coord[0][0],
                                            block_coord[0][1]) or not self.is_point_fits_field(
                block_coord[1][0], block_coord[1][1]):
                return False
            for fall_block in self.fallen_bl:
                if block_coord[0][0] == c.coords(fall_block)[0] and block_coord[0][1] == c.coords(fall_block)[1]:
                    return False
        return True

    def tic(self):
        global speed
        if not self.stop:
            if not self.check_can_move_block(self.figure.fall(test=True)):
                self.save_fallen_block()
                self.delete_line()
                self.score()
                self.new_figure(self.next_id)
                self.get_next_figure()
                if self.col_vse >= 5:
                    self.speed_otn += 100
                    self.col_vse -= 5
                speed = 1000 - self.speed_otn
            else:
                self.figure.fall()
            c.after(speed, self.tic)
        return

    def controlling(self, event):
        if event.keycode == 80:
            if self.stop == False:
                self.stop = True
                self.paysa = c.create_rectangle(border_width / 2, border_width / 2,
                                                columns * cell_size + border_width * 1.5,
                                                rows * cell_size + border_width * 1.5, outline='green',
                                                width=border_width,
                                                fill='white')
                self.paysatext = c.create_text(
            border_width / 2 + columns * cell_size + border_width * 1.5 + cell_size * columns // 4, 200,
            text='pause', justify=CENTER, font="Verdana 20")
            else:
                self.stop = False
                c.delete(self.paysa)
                c.delete(self.paysatext)
        if not self.stop:
            if event.keycode == 40:
                self.figure.drop()
            if event.keycode == 37:
                if self.check_can_move_block(self.figure.left(test=True)):
                    self.figure.left()
            if event.keycode == 39:
                if self.check_can_move_block(self.figure.right(test=True)):
                    self.figure.right()
            if event.keycode == 38:
                if self.check_can_move_block(self.figure.rotate(test=True)):
                    self.figure.rotate()
        if self.stop:
            if event.keycode == 13:
                global speed
                for block in self.fallen_bl:
                    c.delete(block)
                for block in self.figure.blocks:
                    c.delete(block)
                c.delete(self.storetext)
                c.delete(self.endtext)
                c.delete(self.field2)
                c.delete(self.field1)
                speed = 1000
                game = Tetris()
                game.run()
        return

    def get_next_figure(self):
        if not self.stop:
            if self.next_figure:
                self.next_figure.delete_blocks()
            self.next_id = randint(0, 6)
            self.next_figure = Figure(self.next_id, 6.3, 6)
        return

    def new_figure(self, id):
        self.storetext = self.text = c.create_text(
            border_width / 2 + columns * cell_size + border_width * 1.5 + cell_size * columns // 4, 100,
            text='score:\n {}'.format(self.och), justify=CENTER, font="Verdana 20")
        self.figure = Figure(id, 0, 0)
        for fall_block in self.fallen_bl:
            if (c.coords(self.figure.blocks[0])[0] == c.coords(fall_block)[0] and c.coords(self.figure.blocks[0])[1] ==
                c.coords(fall_block)[1]) or (
                    c.coords(self.figure.blocks[1])[0] == c.coords(fall_block)[0] and c.coords(self.figure.blocks[1])[
                1] == c.coords(fall_block)[1]) or (
                    c.coords(self.figure.blocks[2])[0] == c.coords(fall_block)[0] and c.coords(self.figure.blocks[2])[
                1] == c.coords(fall_block)[1]) or (
                    c.coords(self.figure.blocks[3])[0] == c.coords(fall_block)[0] and c.coords(self.figure.blocks[3])[
                1] == c.coords(fall_block)[1]):
                self.stop = True
                self.endtext = c.create_text(border_width / 2 + columns / 3 * 2 * cell_size, border_width / 2 + rows / 2 * cell_size, text='Press Enter to \n start new game', justify=CENTER, font="Verdana 40")
        self.och += 100
        return

    def run(self):
        self.print_field()
        self.new_figure(self.next_id)
        self.get_next_figure()
        self.tic()
        root.mainloop()
        return


game = Tetris()
game.run()
